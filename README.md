# RPC Named Pipes
RPC Named Pipes consists of two C/C++ projects using Windows Named Pipes for interprocess communication.
The both client and the server can send commands/messages in a synchronous and asynchronous way.
As an example of use, the server project stores a list of Users (UserRepository).
Each User hash the following attributes: name, email, value, and valueDouble.
The client can create new Users from a name, modify attributes on the Repository, and list the Users that are in the Repository.

# Releases
The binaries are available in the [binaries folder](https://gitlab.com/xanokothe/RPCNamedPipes/tree/master/binaries).

# Features
  - Native Windows Named Pipes
  - Synchronous and Asynchronous messages
  - Simple message serialization and deserialization
  - Separated Model and Controller

