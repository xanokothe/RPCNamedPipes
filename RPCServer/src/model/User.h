#ifndef USER_H_
#define USER_H_

#include <string>

/*
 *
 */
class User {
public:
    std::string name;
    std::string email;
    int value;
    double valueDouble;

    User();

    /**
     * Returns all fields in string
     * @return string
     */
    std::string toString();

    virtual ~User();
};

#endif /* USER_H_ */
