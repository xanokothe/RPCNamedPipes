#include "User.h"

User::User() {
    this->name = "";
    this->email = "";
    this->value = 0;
    this->valueDouble = 0.0;
}

std::string User::toString() {
    std::string str;
    /*
     str.append(" this->name ").append(this->name);
     str.append(" this->email ").append(this->email);
     str.append(" this->value ").append(std::to_string(this->value));
     str.append(" this->valueDouble ").append(std::to_string(this->valueDouble));
     */
    str.append(" Name: ").append(this->name).append("\n");
    if (this->email.length() > 0) {
        str.append(" Email: ").append(this->email).append("\n");
    }
    if (this->value != 0) {
        str.append(" Value: ").append(std::to_string(this->value)).append("\n");
    }
    if (this->valueDouble != 0) {
        str.append(" ValueDouble: ").append(std::to_string(this->valueDouble)).append("\n");
    }

    return str;
}

User::~User() {

}

