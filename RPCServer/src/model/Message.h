/**
 * @file Message.h
 * 
 *
 * @copyright Este arquivo está sujeito aos termos e condições descritas
 * no arquivo 'LICENSE.txt', que faz parte deste pacote de código-fonte.
 */
#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <string>

/*
 *
 */
class Message {
    bool serializeUInt8(unsigned int value, char **buf, int *size);
    bool serializeString(const std::string *str, char **buf, int *size);

    bool deserializeUInt8(unsigned int* value, const char* buf, int *pos);
    bool deserializeString(std::string* str, const char* buf, int *pos);

public:
    unsigned int type;
    std::string method;
    std::string name;
    std::string param;
    std::string value;

    Message();

    bool serialize(char **buf, int *size);

    bool deserialize(const char *buf, int size);

    std::string toString();

    virtual ~Message();
};

#endif /* MESSAGE_H_ */
