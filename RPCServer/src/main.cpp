#include <stdio.h>
#include <cstring>
#include <string>

#include "controller/ServerThread.h"
#include "model/Message.h"


int main(int argc, char **argv) {
	ServerThread server;

	char line[255] = { 0 };
	char pipe[255] = { 0 };

	printf("---- Server ----\n");
	printf("Enter the name of the pipe (suffix is _x)\n");
	printf("> ");

    scanf("%s", pipe);
    strcat(pipe, "_x");

	std::string pipeName;
	pipeName.append("//.//pipe//").append(pipe);
	printf("Named Pipe started at %s\n", pipeName.c_str());

	server.start(pipeName.c_str());
    server.newUser("robb");
    server.newUser("sansa");
    server.newUser("arya");
    server.newUser("bran");

	printf("Type 'help' for more information\n");
	while (true) {
		printf("> ");
		scanf("%s", line);

		if (strcmp(line, "exit") == 0) {
			break;
		}

		if (strcmp(line, "help") == 0) {
			printf("exit   - Close\n");
			printf("list   - Shows the available objects\n");
			printf("new    - Creates a object\n");
			printf("remove - Removes a object\n");
			printf("set    - Sets a field name and value\n");
            printf("hello  - Sends a hello message\n");
            printf("ping   - Sends a ping and receives pong\n");
		} else if (strcmp(line, "list") == 0) {
			server.printAll();
		} else if(strcmp(line, "new") == 0) {
			printf("Enter the name\n");
			printf("name> ");

			char user[255] = { 0 };
			scanf("%s", user);
			server.newUser(user);
		} else if(strcmp(line, "remove") == 0) {
			char user[255] = { 0 };

			printf("Enter the name\n");
			printf("name> ");
			scanf("%s", user);

			server.removeUser(user);
		} else if(strcmp(line, "set") == 0) {
			char user[255] = { 0 };
			char param[255] = { 0 };
			char value[255] = { 0 };

			printf("Enter the name\n");
			printf("name> ");
			scanf("%s", user);

			printf("Enter the param [name, email, value, valueDouble]\n");
			printf("param> ");
			scanf("%s", param);

			printf("Enter the value\n");
			printf("value> ");
			scanf("%s", value);

			server.setUser(user, param, value);
        } else if(strcmp(line, "hello") == 0) {
            server.sendHello();
        } else if(strcmp(line, "ping") == 0) {
            server.ping();
		} else {
			printf("Invalid command\nType 'help' for more information\n");
		}
	}

	return 0;
}

