#include "UserRepository.h"

#include <iterator>

UserRepository::UserRepository() {

}

bool UserRepository::get(std::string name, User* user) {
    for (std::vector<User*>::iterator b = this->users.begin(); b != this->users.end(); b++) {
        User *u = *b;

        if (u->name.compare(name) == 0) {
            *user = *u;
            return true;
        }
    }

    return false;
}

bool UserRepository::insert(std::string name) {
    User u;
    u.name = name;

    return this->insert(&u);
}

bool UserRepository::insert(User *user) {
    for (std::vector<User*>::iterator b = this->users.begin(); b != this->users.end(); b++) {
        User *u = *b;

        if (u->name.compare(user->name) == 0) {
            return false;
        }
    }

    User *u = new User();
    *u = *user;

    this->users.push_back(u);

    return true;
}

bool UserRepository::remove(std::string name) {
    for (std::vector<User*>::iterator b = this->users.begin(); b != this->users.end();) {
        User *u = *b;
        if (u->name.compare(name) == 0) {
            delete u;
            b = this->users.erase(b);
            return true;
        } else {
            ++b;
        }
    }

    return false;
}

bool UserRepository::save(User* user) {
    for (std::vector<User*>::iterator b = this->users.begin(); b != this->users.end();) {
        User *u = *b;
        if (u->name.compare(user->name) == 0) {
            *u = *user;
            return true;
        } else {
            ++b;
        }
    }

    return this->insert(user);
}

std::vector<User*> UserRepository::findAll() {
    return this->users;
}

UserRepository::~UserRepository() {
    for (std::vector<User*>::iterator b = this->users.begin(); b != this->users.end(); b++) {
        User *u = *b;
        delete u;
    }
}
