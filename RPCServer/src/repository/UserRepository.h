#ifndef USERREPOSITORY_H_
#define USERREPOSITORY_H_

#include <string>
#include <vector>

#include "../model/User.h"

/*
 * DAO for Users
 */
class UserRepository {
    std::vector<User*> users;

public:
    UserRepository();

    /**
     * Inserts a new User by its name
     * @param name User name
     * @return Success or Fail
     */
    bool insert(std::string name);

    /**
     * Inserts a new User
     * @param User User data
     * @return Success or Fail
     */
    bool insert(User *user);

    /**
     * Gets an User by its name
     * @param name User name
     * @param User User data
     * @return Success or Fail
     */
    bool get(std::string name, User *user);

    /**
     * Removes an User by its name
     * @param name User name
     * @return Success or Fail
     */
    bool remove(std::string name);

    /**
     * Edits an User
     * @param User User data
     * @return Success or Fail
     */
    bool save(User *user);

    /**
     * Returns all users
     * @return Vector of Users
     */
    std::vector<User*> findAll();

    virtual ~UserRepository();
};

#endif /* USERREPOSITORY_H_ */
