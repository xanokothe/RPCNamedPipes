/*
 * ClientThread.h
 *
 *  Created on: 5 de jul de 2018
 *      Author: Xano
 */

#ifndef SRC_CONTROLLER_CLIENTTHREAD_H_
#define SRC_CONTROLLER_CLIENTTHREAD_H_

#include <winnt.h>

#include "ServerThread.h"

class ServerThread;

class ClientThread: public Thread {
    HANDLE hPipe;
    ServerThread *server;

public:
    ClientThread();

    void start(ServerThread *server);

    void run();

    void send(const char *data, int size);

    void sendHello();

    void ping();

    virtual ~ClientThread();
};

#endif /* SRC_CONTROLLER_CLIENTTHREAD_H_ */
