/*
 * ServerThread.h
 *
 *  Created on: 5 de jul de 2018
 *      Author: Xano
 */

#ifndef SRC_CONTROLLER_SERVERTHREAD_H_
#define SRC_CONTROLLER_SERVERTHREAD_H_

#include <winnt.h>

#include "../utils/Thread.h"

class ClientThread;

class ServerController;

class ServerThread: public Thread {
    ClientThread *currentClient;
    ServerController *serverController;
    HANDLE hPipe;

public:
    ServerThread();

    void newUser(const char* user);

    void removeUser(const char* user);

    void setUser(const char *name, const char *param, const char *value);

    void printAll();

    void sendHello();

    void ping();

    void start(const char* path);

    void run();

    HANDLE getHPipe();

    ServerController *getServerController();

    virtual ~ServerThread();
};

#endif /* SRC_CONTROLLER_SERVERTHREAD_H_ */
