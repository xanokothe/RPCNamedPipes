#include "UserController.h"

#include <cstdlib>
#include <cstring>
#include <string>

#include "../model/User.h"
#include "../repository/UserRepository.h"

UserController::UserController() {
    this->userRepository = new UserRepository();
}

bool UserController::add(const char* name) {
    return this->userRepository->insert(name);
}

bool UserController::remove(const char* name) {
    return this->userRepository->remove(name);
}

std::string UserController::list() {
    std::string ret;
    std::vector<User*> list = this->userRepository->findAll();

    for (std::vector<User*>::iterator b = list.begin(); b != list.end(); b++) {
        User *u = *b;
        ret.append(u->toString()).append("\n");
    }

    if(ret.length() > 255) {
        //printf("Message too big...");
        ret = ret.substr(0, 255);
    }

    return ret;
}

bool UserController::set(const char *name, const char *param, const char *value) {
    User u;
    if (this->userRepository->get(name, &u)) {
        if (strcmp(param, "name") == 0) {
            this->userRepository->remove(name);
            u.name = value;
            return this->userRepository->insert(&u);
        } else if (strcmp(param, "email") == 0) {
            u.email = value;
            return this->userRepository->save(&u);
        } else if (strcmp(param, "value") == 0) {
            u.value = atoi(value);
            return this->userRepository->save(&u);
        } else if (strcmp(param, "valueDouble") == 0) {
            u.valueDouble = atof(value);
            return this->userRepository->save(&u);
        }
    }

    return false;
}

std::string UserController::get(const char* name, const char* param) {
    User u;

    if (this->userRepository->get(name, &u)) {
        if (strcmp(param, "name") == 0) {
            return u.name;
        } else if (strcmp(param, "email") == 0) {
            return u.email;
        } else if (strcmp(param, "value") == 0) {
            return std::to_string(u.value);
        } else if (strcmp(param, "value2") == 0) {
            return std::to_string(u.valueDouble);
        }
    }

    return "";
}

UserController::~UserController() {
    delete this->userRepository;
}

