#ifndef USERCONTROLLER_H_
#define USERCONTROLLER_H_

#include <string>

class UserRepository;

/*
 *
 */
class UserController {
    UserRepository *userRepository;

public:
    UserController();

    /**
     * Adds a new user
     * @param name User name
     * @return Success or Fail
     */
    bool add(const char *name);

    /**
     * Removes an user
     * @param name User name
     * @return Success or Fail
     */
    bool remove(const char *name);

    /**
     * Returns an user's field by its name
     * @param name User name
     * @param param Field name
     * @param
     */
    std::string get(const char *name, const char *param);

    /**
     * Sets an user's field by its name
     * @param name User name
     * @param param Field name
     * @param value String value
     */
    bool set(const char *name, const char *param, const char *value);

    /**
     * Return all users
     * @return users in string format
     */
    std::string list();

    virtual ~UserController();
};


#endif /* USERCONTROLLER_H_ */
