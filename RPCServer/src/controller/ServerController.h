#ifndef SERVERCONTROLLER_H_
#define SERVERCONTROLLER_H_

#include <string>

class Message;
class UserController;

/*
 * Handlers the server commands
 */
class ServerController {
    UserController *userController;

    /**
     * @param method Method Name
     * @param value String Value
     * @param pck Response Data
     * @param size Response Data Size
     * @return Success or fail
     */
    bool pack(const char *method, const char *value, char **pck, int *size);

public:
    ServerController();

    /**
     * @param pck Data
     * @param size Data Size
     * @return Success or fail
     */
    bool packetRcv(const char *pck, int size);

    /**
     * @param pck Data
     * @param size Data Size
     * @param resp Response Data
     * @param resp_size Response Data Size
     * @return Success or fail
     */
    bool packetRcv(const char *pck, int pck_size, char **resp, int *resp_size);


    /**
     * @param Message Message
     * @param resp Response Data
     * @param resp_size Response Data Size
     * @return Success or fail
     */
    bool commandRcv(const Message *msg, char **resp, int *resp_size);

    /**
     * List all users
     */
    std::string list();

    /**
     * Add user
     */
    void newUser(const char *name);

    /**
     * Remove user
     */
    void removeUser(const char *name);

    /**
     * Edit user
     */
    void setUser(const char *name, const char *param, const char *value);

    virtual ~ServerController();
};

#endif /* SERVERCONTROLLER_H_ */
