/*
 * ServerThread.cpp
 *
 *  Created on: 5 de jul de 2018
 *      Author: Xano
 */

#include "ServerThread.h"

#include <stdio.h>
#include <winbase.h>
#include <windef.h>
#include <winerror.h>

#include "ClientThread.h"
#include "ServerController.h"

#define BUFSIZE 512

ServerThread::ServerThread() {
    this->currentClient = 0;
    this->hPipe = 0;
    this->serverController = 0;
}

void ServerThread::ping() {
    if (this->currentClient) {
        this->currentClient->ping();
    }
}

void ServerThread::sendHello() {
    if (this->currentClient) {
        this->currentClient->sendHello();
    }
}

void ServerThread::printAll() {
    std::string list = this->serverController->list();
    if (list.length() > 0) {
        printf("%s", list.c_str());
    } else {
        printf("List is empty\n");
    }
}

void ServerThread::newUser(const char *name) {
    this->serverController->newUser(name);
}

void ServerThread::removeUser(const char *name) {
    this->serverController->removeUser(name);
}

void ServerThread::setUser(const char *name, const char *param, const char *value) {
    this->serverController->setUser(name, param, value);
}

void ServerThread::start(const char *path) {
    this->serverController = new ServerController();
    this->hPipe = CreateNamedPipe(path, PIPE_ACCESS_DUPLEX, PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT, PIPE_UNLIMITED_INSTANCES, BUFSIZE, BUFSIZE, 0, NULL);
    Thread::start();
}

void ServerThread::run() {
    BOOL fConnected;

    while (1) {
        fConnected =
                ConnectNamedPipe(hPipe, NULL) ? TRUE : (GetLastError() == ERROR_PIPE_CONNECTED);

        printf("Client connected #%d\n", fConnected);

        if (fConnected) {
            currentClient = new ClientThread();
            currentClient->start(this);
            currentClient->join();

            delete currentClient;
            currentClient = 0;
        } else {
            break;
        }
    }
}

ServerController * ServerThread::getServerController() {
    return this->serverController;
}

HANDLE ServerThread::getHPipe() {
    return this->hPipe;
}

ServerThread::~ServerThread() {
    delete this->serverController;
}

