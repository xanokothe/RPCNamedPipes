/**
 * @file ServerController.cpp
 * 
 *
 * @copyright Este arquivo está sujeito aos termos e condições descritas
 * no arquivo 'LICENSE.txt', que faz parte deste pacote de código-fonte.
 */

#include "ServerController.h"

#include <cstdio>
#include <cstring>
#include <string>

#include "../model/Message.h"
#include "UserController.h"

ServerController::ServerController() {
    this->userController = new UserController();
}

bool ServerController::packetRcv(const char* pck, int size) {
    return this->packetRcv(pck, size, 0, 0);
}

bool ServerController::packetRcv(const char* pck, int pck_size, char** resp, int *resp_size) {
    if (pck_size <= 0) {
        return false;
    }

    if (pck[0] == 1) {
        Message message;
        message.deserialize(pck, pck_size);

        return this->commandRcv(&message, resp, resp_size);
    }

    return false;
}

void ServerController::newUser(const char *name) {
    this->userController->add(name);
}

void ServerController::removeUser(const char *name) {
    this->userController->remove(name);
}

void ServerController::setUser(const char *name, const char *param, const char *value) {
    this->userController->set(name, param, value);
}

std::string ServerController::list() {
    return this->userController->list();
}

bool ServerController::pack(const char *method, const char *value, char **pck, int *size) {
    Message message;
    message.method = method;
    if (value) {
        message.value = value;
    }

    return message.serialize(pck, size);
}

bool ServerController::commandRcv(const Message *msg, char** resp, int *resp_size) {
    const char *method = msg->method.c_str();
    const char *user = msg->name.c_str();
    const char *param = msg->param.c_str();
    const char *value = msg->value.c_str();

    bool useRet = resp != 0 && resp_size != 0;

    if (strcmp(method, "add") == 0) {
        bool res = this->userController->add(user);
        if (res) {
            if (useRet) {
                this->pack("result", "1", resp, resp_size);
            }
        }

        return res;
    } else if (strcmp(method, "remove") == 0) {
        bool res = this->userController->remove(user);

        if (res) {
            if (useRet) {
                this->pack("result", "1", resp, resp_size);
            }
        }

        return res;
    } else if (strcmp(method, "set") == 0) {
        bool res = this->userController->set(user, param, value);

        if (res) {
            if (useRet) {
                this->pack("result", "1", resp, resp_size);
            }
        }

        return res;
    } else if (strcmp(method, "get") == 0) {
        std::string v = this->userController->get(user, param);

        if (useRet) {
            this->pack("result", v.c_str(), resp, resp_size);
        }

        return true;
    } else if (strcmp(method, "list") == 0) {
        std::string v = this->userController->list();

        if (useRet) {
            this->pack("result", v.c_str(), resp, resp_size);
        }

        return true;
    } else if (strcmp(method, "hello") == 0) {
        printf("%s said hello\n", user);

        return true;
    } else if (strcmp(method, "ping") == 0) {
        if (useRet) {
            this->pack("pong", 0, resp, resp_size);
        }

        return true;
    } else if (strcmp(method, "pong") == 0) {
        printf("pong\n");

        return true;
    }

    this->pack("method_not_found", method, resp, resp_size);
    return false;
}

ServerController::~ServerController() {
    delete this->userController;
}
