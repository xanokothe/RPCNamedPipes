/*
 * ClientThread.cpp
 *
 *  Created on: 5 de jul de 2018
 *      Author: Xano
 */

#include "ClientThread.h"

#include <stdio.h>
#include <stdlib.h>
#include <winbase.h>
#include <windef.h>
#include <winerror.h>
#include <string>

#include "../model/Message.h"
#include "ServerController.h"

#define BUFSIZE 512

ClientThread::ClientThread() {
	this->hPipe = 0;
	this->server = 0;
}

void ClientThread::start(ServerThread *server) {
	this->server = server;
	this->hPipe = this->server->getHPipe();
	Thread::start();
}

void ClientThread::run() {
	this->sendHello();

	char request[BUFSIZE];

	while (1) {
		DWORD bytesRead = 0;
		DWORD bytesAvail = 0;
		BOOL fSuccess = FALSE;

		if (!PeekNamedPipe(this->hPipe, NULL, 0, NULL, &bytesAvail, NULL)) {
			if (GetLastError() == ERROR_BROKEN_PIPE) {
				printf("Client disconnected\n",
						GetLastError());
			} else {
				printf("PeekNamedPipe error %d.\n", GetLastError());
			}
			break;
		}

		if (bytesAvail > 0) {
			fSuccess = ReadFile(hPipe, request, BUFSIZE, &bytesRead, NULL);

			if (!fSuccess || bytesRead == 0) {
				if (GetLastError() == ERROR_BROKEN_PIPE) {
					printf("Client disconnected\n",
							GetLastError());
				} else {
					printf("InstanceThread ReadFile failed, GLE=%d.\n",
							GetLastError());
				}
				break;
			}

			Message msgRead;
			msgRead.deserialize(request, bytesRead);

			char *data = 0;
			int size = 0;

			if(this->server->getServerController()->commandRcv(&msgRead, &data, &size)) {
				this->send(data, size);
			}

			if(data) {
				free(data);
			}
		} else {
			Sleep(10);
		}
	}

	FlushFileBuffers(hPipe);
	DisconnectNamedPipe(hPipe);
}

void ClientThread::send(const char *data, int size) {
	if(!data || size == 0) {
		return;
	}

	DWORD written;
	BOOL fSuccess;

	fSuccess = WriteFile(hPipe, data, size, &written, NULL);

	if (!fSuccess) {
		printf("InstanceThread WriteFile failed, GLE=%d.\n", GetLastError());
	}
}

void ClientThread::sendHello() {
	char *data = 0;
	int size = 0;
	DWORD written;
	BOOL fSuccess;

	Message msgReply;
	msgReply.method = "hello";
	msgReply.name = "server";

	msgReply.serialize(&data, &size);

	fSuccess = WriteFile(hPipe, data, size, &written, NULL);

	if (!fSuccess) {
		printf("InstanceThread WriteFile failed, GLE=%d.\n", GetLastError());
	}

	if (data) {
		free(data);
	}
}

void ClientThread::ping() {
    char *data = 0;
    int size = 0;
    DWORD written;
    BOOL fSuccess;

    Message msgReply;
    msgReply.method = "ping";

    msgReply.serialize(&data, &size);

    fSuccess = WriteFile(hPipe, data, size, &written, NULL);

    if (!fSuccess) {
        printf("InstanceThread WriteFile failed, GLE=%d.\n", GetLastError());
    }

    if (data) {
        free(data);
    }
}

ClientThread::~ClientThread() {

}

