/*
 * Thread.h
 *
 *  Created on: 5 de jul de 2018
 *      Author: Xano
 */

#ifndef UTILS_THREAD_H_
#define UTILS_THREAD_H_

#include <windef.h>
#include <winnt.h>

class Thread {
    HANDLE handle;
    DWORD threadID;

public:
    Thread();

    virtual void start();

    virtual void run();

    virtual void join();

    virtual ~Thread();
};

#endif /* UTILS_THREAD_H_ */
