/*
 * Thread.cpp
 *
 *  Created on: 5 de jul de 2018
 *      Author: Xano
 */

#include "Thread.h"

#include <winbase.h>

#include "../utils/Thread.h"

static DWORD WINAPI threadFunction(LPVOID lpParameter) {
    Thread *thread = (Thread*) lpParameter;
    thread->run();

    return 0;
}

Thread::Thread() {
    this->handle = 0;
    this->threadID = 0;
}

void Thread::run() {

}

void Thread::start() {
    this->handle = CreateThread(0, 0, threadFunction, this, 0, &this->threadID);
}

void Thread::join() {
    WaitForSingleObject(this->handle, INFINITE);
}

Thread::~Thread() {
    CloseHandle(this->handle);
}

