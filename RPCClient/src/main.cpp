#include <stdio.h>
#include <cstring>
#include <string>

#include "controller/ClientPipe.h"

int main(int argc, char **argv) {
    char line[255] = { 0 };
    char pipe[255] = { 0 };

    ClientPipe client;

    printf("---- Client ----\n");
    printf("Enter the name of the pipe (suffix is _x)\n");
    printf("> ");

    scanf("%s", pipe);
    strcat(pipe, "_x");

    std::string pipeName;
    pipeName.append("//.//pipe//").append(pipe);
    printf("Connecting to named pipe %s\n", pipeName.c_str());
    client.start(pipeName.c_str());

    printf("Type 'help' for more information\n");
    while (true) {
        printf("> ");
        scanf("%s", line);

        if (strcmp(line, "exit") == 0) {
            break;
        }

        if (strcmp(line, "help") == 0) {
            printf("exit   - Close\n");
            printf("list   - Shows the available objects\n");
            printf("new    - Creates a object\n");
            printf("remove - Removes a object\n");
            printf("set    - Sets a field name and value\n");
            printf("hello  - Sends a hello message\n");
            printf("ping   - Sends a ping and receives pong\n");
        } else if (strcmp(line, "list") == 0) {
            std::string list = client.getList();
            printf("%s\n", list.c_str());
        } else if(strcmp(line, "new") == 0) {
            printf("Enter the name\n");
            printf("name> ");

            char user[255] = { 0 };
            scanf("%s", user);
            client.newUser(user);
        } else if(strcmp(line, "remove") == 0) {
            char user[255] = { 0 };

            printf("Enter the name\n");
            printf("name> ");
            scanf("%s", user);

            client.removeUser(user);
        } else if(strcmp(line, "set") == 0) {
            char user[255] = { 0 };
            char param[255] = { 0 };
            char value[255] = { 0 };

            printf("Enter the name\n");
            printf("name> ");
            scanf("%s", user);

            printf("Enter the param [name, email, value, valueDouble]\n");
            printf("param> ");
            scanf("%s", param);

            printf("Enter the value\n");
            printf("value> ");
            scanf("%s", value);

            client.setUser(user, param, value);
        } else if(strcmp(line, "hello") == 0) {
            client.sendHello();
        } else if(strcmp(line, "ping") == 0) {
            client.ping();
        } else {
            printf("Invalid command\nType 'help' for more information\n");
        }
    }

    return 0;
}
