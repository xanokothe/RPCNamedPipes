/**
 * @file Message.cpp
 * 
 *
 * @copyright Este arquivo está sujeito aos termos e condições descritas
 * no arquivo 'LICENSE.txt', que faz parte deste pacote de código-fonte.
 */

#include "Message.h"

#include <stdlib.h>
#include <cstring>

Message::Message() {
    this->type = 1;
    this->method = "";
    this->name = "";
    this->param = "";
    this->value = "";
}

bool Message::serializeUInt8(unsigned int value, char **buf, int *size) {
    char *p = *buf;

    *p = value;
    *size = *size + 1;

    return true;
}

bool Message::serializeString(const std::string *value, char **buf, int *size) {
    std::string tmp = *value;
    int len = tmp.length();
    if (len > 255) {
        len = 255;
        tmp = tmp.substr(0, 255);
    }

    char *p = *buf;
    int newSize = *size;
    p[newSize] = len;
    newSize++;

    memcpy(&p[newSize], tmp.c_str(), len);
    newSize += len;

    *size = newSize;

    return true;
}

bool Message::serialize(char** buf, int* size) {
    int methodSize = this->method.length();
    int nameSize = this->name.length();
    int paramSize = this->param.length();
    int valueSize = this->value.length();

    if (methodSize > 255 || nameSize > 255 || paramSize > 255 || valueSize > 255) {
        return false;
    }

    if (*buf == 0) {
        *buf = (char*) calloc(1, 6 + methodSize + nameSize + paramSize + valueSize);
    }

    this->serializeUInt8(this->type, buf, size);
    this->serializeString(&this->method, buf, size);
    this->serializeString(&this->name, buf, size);
    this->serializeString(&this->param, buf, size);
    this->serializeString(&this->value, buf, size);

    return true;
}

bool Message::deserializeUInt8(unsigned int* value, const char* buf, int *pos) {
    *value = buf[*pos] & 0xFF;
    *pos += 1;

    return true;
}

bool Message::deserializeString(std::string* str, const char* buf, int *pos) {
    char tmp[256] = { 0 };
    int newPos = *pos;
    int len = buf[newPos] & 0xFF;
    newPos++;

    memcpy(tmp, &buf[newPos], len);
    *str = tmp;
    newPos += len;

    *pos = newPos;

    return true;
}

bool Message::deserialize(const char* buf, int size) {
    if(buf == 0 || size <= 4) {
        return false;
    }

    int pos = 0;

    this->deserializeUInt8(&this->type, buf, &pos);
    this->deserializeString(&this->method, buf, &pos);
    this->deserializeString(&this->name, buf, &pos);
    this->deserializeString(&this->param, buf, &pos);
    this->deserializeString(&this->value, buf, &pos);

    return pos == size;
}

std::string Message::toString() {
    std::string ret;
    /*
    ret.append("this->type = ").append(std::to_string(this->type)).append(" ");
    ret.append("this->method = ").append(this->method).append(" ");
    ret.append("this->name = ").append(this->name).append(" ");
    ret.append("this->param = ").append(this->param).append(" ");
    ret.append("this->value = ").append(this->value);*/
    ret.append("Method: ").append(this->method).append("\n");
    ret.append("Name: ").append(this->name).append("\n");
    ret.append("Param: ").append(this->param).append("\n");
    ret.append("Value: ").append(this->value).append("\n");

    return ret;
}

Message::~Message() {

}

