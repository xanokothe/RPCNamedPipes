/*
 * ClientController.cpp
 *
 *  Created on: Jul 6, 2018
 *      Author: X
 */

#include <stdio.h>

#include "../model/Message.h"
#include "ClientController.h"

ClientController::ClientController() {

}

bool ClientController::pack(const char *method, const char *value, char **pck, int *size) {
    Message message;
    message.method = method;
    if (value) {
        message.value = value;
    }

    return message.serialize(pck, size);
}

bool ClientController::commandRcv(Message *msg, char** resp, int *resp_size) {
    bool useRet = resp != 0 && resp_size != 0;

    if (msg->method.compare("result") == 0) {
        printf("Result:\n");
        if (msg->value.length() > 0) {
            printf("%s\n", msg->value.c_str());
        } else {
            printf("result empty\n");
        }
        printf("> ");
    } else if (msg->method.compare("hello") == 0) {
        printf("%s said hello\n", msg->name.c_str());
        printf("> ");
    } else if (msg->method.compare("ping") == 0) {
        if (resp) {
            if (useRet) {
                this->pack("pong", 0, resp, resp_size);
                return true;
            }
        }
    } else if (msg->method.compare("pong") == 0) {
        printf("pong\n");
        printf("> ");
    }

    return false;
}

ClientController::~ClientController() {

}

