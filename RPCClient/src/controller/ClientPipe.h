/*
 * ClientPipe.h
 *
 *  Created on: 6 de jul de 2018
 *      Author: Xano
 */

#ifndef SRC_CONTROLLER_CLIENTPIPE_H_
#define SRC_CONTROLLER_CLIENTPIPE_H_

#include <winnt.h>
#include <string>

#include "../utils/Thread.h"

class ClientController;

class Message;

class ClientPipe: public Thread {
    HANDLE hPipe;
    std::string pipeName;
    ClientController *clientController;

public:
    ClientPipe();

    /*
     * Starts the thread with a pipe name
     */
    void start(const char *pipeName);

    /**
     * Sends a message
     * @param msg Message
     */
    void send(Message *msg);

    /**
     * Sends message as bytes
     * @param buf Message
     * @param size Message size
     */
    void send(const char *buf, int size);

    /**
     * Thread running function
     */
    void run();

    /**
     * Sends hello
     */
    void sendHello();

    /**
     * Creates a new user
     * @param name User name
     */
    void newUser(const char *name);

    /*
     * Removes a user
     * @param name User name
     */
    void removeUser(const char *name);

    /*
     * Edits a user
     * @param name User name
     * @param param User field
     * @param value New value for the field
     */
    void setUser(const char *name, const char *param, const char *value);

    /**
     * Sends a ping and waits for a pong
     */
    void ping();

    /**
     * Returns the list of users as strings
     */
    std::string getList();

    virtual ~ClientPipe();
};

#endif /* SRC_CONTROLLER_CLIENTPIPE_H_ */
