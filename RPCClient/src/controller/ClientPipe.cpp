/*
 * ClientPipe.cpp
 *
 *  Created on: 6 de jul de 2018
 *      Author: Xano
 */

#include "ClientPipe.h"

#include <winbase.h>
#include <windef.h>
#include <winerror.h>
#include <winnt.h>
#include <cstdio>
#include <string>

#include "../model/Message.h"
#include "ClientController.h"

#define BUFSIZE 512

ClientPipe::ClientPipe() {
    this->hPipe = 0;
    this->pipeName = "";
    this->clientController = 0;
}

void ClientPipe::start(const char *pipeName) {
    this->pipeName = pipeName;
    this->clientController = new ClientController();

    Thread::start();
}

void ClientPipe::run() {
    char request[BUFSIZE];

    DWORD dwMode;
    BOOL fSuccess;

    this->hPipe = CreateFile(pipeName.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

    if (hPipe == INVALID_HANDLE_VALUE) {
        printf(">>>> Invalid Named Pipe\n");
        system("PAUSE");
        exit(0);
        return;
    }

    dwMode = PIPE_READMODE_MESSAGE;
    fSuccess = SetNamedPipeHandleState(hPipe, &dwMode, NULL, NULL);
    if (!fSuccess) {
        //printf("SetNamedPipeHandleState failed. GLE=%d\n", GetLastError());
        return;
    }

    sendHello();

    while (1) {
        DWORD bytesRead = 0;
        DWORD bytesAvail = 0;
        BOOL fSuccess = FALSE;
        if (!PeekNamedPipe(this->hPipe, NULL, 0, NULL, &bytesAvail, NULL)) {
            printf("Server disconnected\n");
            break;
        }

        if (bytesAvail > 0) {
            fSuccess = ReadFile(hPipe, request, BUFSIZE, &bytesRead, NULL);

            if (!fSuccess || bytesRead == 0) {
                if (GetLastError() == ERROR_BROKEN_PIPE) {
                    printf("Server disconnected\n");
                } else {
                    //printf("InstanceThread ReadFile failed, GLE=%d.\n", GetLastError());
                }
                break;
            }

            char *data = 0;
            int size = 0;
            Message msgRead;
            if (msgRead.deserialize(request, bytesRead)) {
                if (clientController->commandRcv(&msgRead, &data, &size)) {
                    this->send(data, size);
                }

                if (data) {
                    free(data);
                }
            }
        } else {
            Sleep(10);
        }
    }
    Sleep(1000);

    FlushFileBuffers(hPipe);
    DisconnectNamedPipe(hPipe);
}

void ClientPipe::send(Message *msg) {
    char *buf = 0;
    int size = 0;
    if (msg->serialize(&buf, &size)) {
        this->send(buf, size);
        if (buf) {
            free(buf);
        }
    } else {
        printf("Failed to send\n");
    }
}

void ClientPipe::send(const char *buf, int size) {
    DWORD cbWritten;

    WriteFile(hPipe, buf, size, &cbWritten, NULL);
}

void ClientPipe::sendHello() {
    Message msg;
    msg.method = "hello";
    msg.name = "client";

    this->send(&msg);
}

void ClientPipe::newUser(const char* name) {
    Message msg;
    msg.method = "add";
    msg.name = name;

    this->send(&msg);
}

void ClientPipe::removeUser(const char* name) {
    Message msg;
    msg.method = "remove";
    msg.name = name;

    this->send(&msg);
}

void ClientPipe::setUser(const char* name, const char* param, const char* value) {
    Message msg;
    msg.method = "set";
    msg.name = name;
    msg.param = param;
    msg.value = value;

    this->send(&msg);
}

void ClientPipe::ping() {
    BOOL fSuccess;
    Message msg;
    msg.method = "ping";

    this->send(&msg);

    char request[BUFSIZE];
    DWORD bytesRead = 0;
    fSuccess = ReadFile(hPipe, request, BUFSIZE, &bytesRead, NULL);

    if (!fSuccess || bytesRead == 0) {
        return;
    }

    Message msgRead;
    if (msgRead.deserialize(request, bytesRead)) {
        printf("%s\n", msgRead.method.c_str());
    }
}

std::string ClientPipe::getList() {
    BOOL fSuccess;
    Message msg;
    msg.method = "list";

    this->send(&msg);

    char request[BUFSIZE];
    DWORD bytesRead = 0;
    fSuccess = ReadFile(hPipe, request, BUFSIZE, &bytesRead, NULL);

    if (!fSuccess || bytesRead == 0) {
        return "failed";
    }

    Message msgRead;
    if (!msgRead.deserialize(request, bytesRead)) {
        return "failed";
    }

    if (msgRead.method.compare("result") != 0) {
        return "failed";
    }

    return msgRead.value;
}

ClientPipe::~ClientPipe() {

}
