/*
 * ClientController.h
 *
 *  Created on: Jul 6, 2018
 *      Author: X
 */

#ifndef SRC_CONTROLLER_CLIENTCONTROLLER_H_
#define SRC_CONTROLLER_CLIENTCONTROLLER_H_

class Message;

class ClientController {
public:
	ClientController();

	/*
	 * Process a message and returns a response
	 * @param msg Message
	 * @param resp Response
	 * @param resp_size Response size
	 * @return True if it has a response
	 */
	bool commandRcv(Message *msg, char** resp, int *resp_size);

	/**
	 * Creates a message with method and value
	 * @param method Method name
	 * @param value Value
	 * @param pck Message pointer
	 * @param size Message size
	 */
	bool pack(const char *method, const char *value, char **pck, int *size);

	virtual ~ClientController();
};

#endif /* SRC_CONTROLLER_CLIENTCONTROLLER_H_ */
